import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); 
		Random rand = new Random(); 
		int number =rand.nextInt(100); 
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); 
		int x=0;
		
		
		do{
		
		if (guess==number){
			System.out.println("Congratulations!");
		}else{
			if (guess<number){
			System.out.println("Sorry!");
			System.out.println("Mine is greater than your guess.");
			System.out.print("Type -1 to quit or guess another: ");
			x=x+1;
			}else{
			System.out.println("Sorry!");
			System.out.println("Mine is less than your guess.");
			System.out.print("Type -1 to quit or guess another: ");
			x=x+1;
			}guess = reader.nextInt();
			}
		}while((guess!=number)&&(guess!=-1));

		if (guess==number){
			System.out.println("Congratulations! You won after " + x + " attempts!");
		}

		if(guess==-1){
		System.out.println("Sorry, the number was "  + number);
		}
		
		reader.close(); 
	}
	
	
}
