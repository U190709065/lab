import java.util.Scanner;
public class FindPrimes{
	public static boolean isPrime(int number){
		for(int i =2; i<number; i++){
			if (number%i==0){
				return false;
			}
		}
		return true;
	}
	public static void main (String [] args){
		int value=Integer.parseInt(args[0]);
		for(int i=2; i<value; i++){
			if (isPrime(i)){
				System.out.print(i + ",");
			}
		
		}
	System.out.println();
	}
}
	
